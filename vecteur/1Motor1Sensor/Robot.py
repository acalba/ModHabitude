from Vecteur import *
from Environnement import *
import random

class Robot:
    #Constructeur de Robot
    def __init__(self,p,v):
        self.position = p
        self.vitesse = v
        self.environnement = Environnement()
        self.sensimotor = Vecteur(v.x,1/(1+self.position.x**2))
        self.acc = Vecteur(0,0)

    #Actualise le poid des noeuds puis change la position du robot selon la vitesse de ce dernier
    def deplacement (self,DeltaTemps):
        self.environnement.calculPoid(self.sensimotor)
        self.position +=   Vecteur(self.vitesse.x*DeltaTemps,self.vitesse.y*DeltaTemps)
        
    #Deplacement selon les noeuds de l'environnement
    def deplacementIDSM(self,DeltaTemps):
        #Calcul de l'acceleration selon les noeuds 
        self.acc = self.environnement.influenceTt(self.sensimotor)
        self.vitesse += Vecteur(self.acc.x*DeltaTemps,self.acc.y*DeltaTemps)
        self.deplacement(DeltaTemps)
        #Calcul de sensimotor servant au deplacement IDSM
        self.sensimotor = Vecteur(self.vitesse.x,1/(1+self.position.x**2))



    #Deplacement selon le Vecteur v donnée
    def deplacementEntrainement(self, v, DeltaTemps):
        #Calcul de l'acceleration difference vitesse actuel vitesse  
        self.acc = (v - self.vitesse).div(DeltaTemps)
        self.vitesse =  v
        if(self.environnement.calculDensite(self.sensimotor)<1):
            noeud = Noeud(self.position,self.vitesse,self.sensimotor,self.acc,0)
            self.environnement.ajouterNoeud(noeud)
        self.deplacement(DeltaTemps)
        #Calcul de sensimotor servant au deplacement IDSM
        self.sensimotor = Vecteur(self.vitesse.x,1/(1+self.position.x**2))

        
"""
robot = Robot(Vecteur(0,0),Vecteur(0.1,0))

for j in range (0,2):
   
    print()
    print("Passage ",j)
    print("Coord1: ",robot.position.affichage())
    print("Vitesse1: ", robot.vitesse.affichage())
    for i in range (0,10):
        robot.deplacement()
        print("Coord2: ",robot.position.affichage())
        print("Vitesse2: ", robot.vitesse.affichage())
    robot.position = Vecteur(0,0)
    robot.vitesse = Vecteur(0,0.1)

for g in range (0,2):
    robot.position = Vecteur(0,0)
    robot.vitesse = Vecteur(0,0)
    print()
    print("Passage ",j+g)
    for i in range (0,10):   
        robot.deplacement()
        print("Coord: ",robot.position.affichage())
        print("Vitesse: ", robot.vitesse.affichage())
    


robot.environnement.affichageNoeuds()"""
