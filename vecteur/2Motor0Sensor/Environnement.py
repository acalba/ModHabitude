from Noeud import *

class Environnement:
    #Constructeur de l'environnement
    def __init__(self):
        self.noeuds = []

    #Ajoute le noeud en parametre dans la liste des noeuds
    def ajouterNoeud(self,noeud):
        self.noeuds.append(noeud)

    #Calcul la densite des noeuds autour de l'entité
    def calculDensite(self,x):
        rep = 0;
        for n in self.noeuds:
            rep += n.poidFacteur() * n.distanceFacteur(x)
        return rep

    #Calcul l'influence des noeuds sur l'entité
    def influenceTt(self,x):
        rep = Vecteur(0,0);
        if(self.calculDensite(x)==0):
            return rep
        for n in self.noeuds:
            if(n.time>10):
                a = n.position-x
                r= a*(n.vitesse.vecteurUnitaire())*(n.vitesse.vecteurUnitaire())
                r2 = a-r
                rep +=(n.poidFacteur() * n.distanceFacteur(x) * (n.vitesse+r2))

        rep = (1/self.calculDensite(x))*rep
        return rep

    #Change le poid des noeuds selon le placement de l'entité
    def calculPoid(self,x, DeltaTemps):
        for n in self.noeuds:
            r = 10*n.distanceFacteur(x)
            n.poid += DeltaTemps*(-1+r)
            n.time +=DeltaTemps

    #Affichage des noeuds 
    def affichageNoeuds(self):
        for n in self.noeuds:
            print(n.affichage())
        
"""
test = Environnement()
position = Vecteur(1,1)
vitesse = Vecteur(1,1)
noeud1 = Noeud(position,vitesse,0)
test.ajouterNoeud(noeud1)

position = Vecteur(1.1,1.1)
vitesse = Vecteur(1,1)
noeud2 = Noeud(position,vitesse,-1)
test.ajouterNoeud(noeud2)

position = Vecteur(0.9,0.9)
vitesse = Vecteur(1,1)
noeud3 = Noeud(position,vitesse,1)
test.ajouterNoeud(noeud3)

position = Vecteur(1,0.9)
vitesse = Vecteur(1,1)
noeud4 = Noeud(position,vitesse,1)
test.ajouterNoeud(noeud4)

position = Vecteur(0.9,1)
vitesse = Vecteur(1,1)
noeud4 = Noeud(position,vitesse,1)
test.ajouterNoeud(noeud4)

newPosition = Vecteur(1,1)
print("densité: ",test.calculDensite(newPosition))
print("influence: ", test.influenceTt(newPosition).affichage())
"""
