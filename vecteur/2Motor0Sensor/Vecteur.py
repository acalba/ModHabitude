from math import *

class Vecteur:
    def __init__(self,x,y):
        self.x=x
        self.y=y

    def affichage(self):
        return '('+str(self.x)+';'+str(self.y)+')'

    def norme(self):
        return hypot(self.x,self.y)

    def __add__(self,v):
        return Vecteur(self.x+v.x,self.y+v.y)

    def __sub__(self,v):
        return Vecteur(self.x-v.x,self.y-v.y)

    def __rmul__(self,r):
        return Vecteur(self.x*r,self.y*r)

    def __mul__(self,v):
        return self.x*v.x+self.y*v.y
    
    def div(self,r):
        return Vecteur(self.x/r,self.y/r)
	
    def colin(self,v):
        return self.x*v.y==self.y*v.x

    def ortho(self,v):
        return self*v==0
    
    def normal(self):
        return sqrt(self.x**2+self.y**2)

    def vecteurUnitaire(self):
        if(self.normal() == 0):
            return Vecteur(0.0,0.0)
        return self.div(self.normal())
"""
a=Point(-1,3.0)
b=Point(5,1)
c=Point(1,5)

u=c.vecteur(a)
v=c.vecteur(b)

print(u*v)
print(u.ortho(v))
"""
