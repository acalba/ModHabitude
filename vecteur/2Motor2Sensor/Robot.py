from Vecteur import *
from Environnement import *
from math import *
import random

class Robot:
    beta = pi/3
    r = 0.25
    
    #Constructeur de Robot
    def __init__(self,p,v):
        self.position = p # Position du robot
        self.vitesse = v # Vitesse du robot
        self.environnement = Environnement() #Environnement créer pas le robot
        self.alpha = 0 #Orientation du robot
        self.sensimotor = Vecteur(Vecteur(v.x,v.y),Vecteur(self.instensiteLumineuse("false"),self.instensiteLumineuse("true")))
        self.acc = Vecteur(0,0) #Acceleration du robot

    #Actualise le poid des noeuds puis change la position du robot selon la vitesse de ce dernier
    def deplacement (self,DeltaTemps):
        self.environnement.calculPoid(self.sensimotor)
        if(self.environnement.calculDensite(self.sensimotor)<1):
            noeud = Noeud(self.position,self.vitesse,self.sensimotor,self.acc,0)
            self.environnement.ajouterNoeud(noeud)
 
        self.position.x += (cos(self.alpha)*(self.vitesse.x+self.vitesse.y))*DeltaTemps
        self.position.y += (sin(self.alpha)*(self.vitesse.x+self.vitesse.y))*DeltaTemps
        self.alpha += 2(self.vitesse.y-self.vitesse.x)*DeltaTemps

        
    #Deplacement selon les noeuds de l'environnement
    def deplacementIDSM(self,DeltaTemps):
        #Calcul de l'acceleration selon les noeuds 
        self.acc = self.environnement.influenceTt(self.sensimotor)
        self.vitesse += self.acc
        self.deplacement(DeltaTemps)
        #Calcul de sensimotor servant au deplacement IDSM
        self.sensimotor = Vecteur(self.vitesse.x,1/(1+self.position.x**2))


    #Deplacement selon le Vecteur v donnée
    def deplacementEntrainement(self, v, DeltaTemps):
        #Calcul de l'acceleration difference vitesse actuel vitesse  
        self.acc = (v - self.vitesse)
        self.vitesse =  v
        self.deplacement(DeltaTemps)
        #Calcul de sensimotor servant au deplacement IDSM
        self.sensimotor = Vecteur(self.vitesse.x,1/(1+self.position.x**2))


    #Renvoie l'intensité lumineuse que ressent le capteur    
    def instensiteLumineuse(self,cote):
        beta = pi/3
        r = 0.25
        
        b = Vecteur(cos(self.alpha+beta),sin(self.alpha+beta))
        c = Vecteur(0,0)#A changer selon direction du vecteur
        if (cote=="true"):
            d = Vecteur(self.position.x+(r*cos(self.alpha+beta)),self.position.y+(r*sin(self.alpha+beta))).normal()
        else :
            d = Vecteur(self.position.x+(r*cos(self.alpha-beta)),self.position.y+(r*sin(self.alpha-beta))).normal()
        lumiere = ((Vecteur(b.x*c.normal(),b.y*c.normal())).div(1+d*d))
        return lumiere
