from Robot import *
from math import *
import random
import matplotlib.pyplot as plt

DeltaTemps = 1


print("Debut Entrainement")
Robot = Robot(Vecteur(0,0),Vecteur(0.001,0.001))
lDeplRobXA = []
lDeplRobYA = []
lVRobXA = []
lVRobYA = []
x=0.1
y=0.1
lDeplRobXA.append(Robot.position.x)
lDeplRobYA.append(Robot.position.y)
for i in range(0,1000):
    #Cercle
    #"""
    x = 0.075*cos(2*pi/10*(i*0.1))
    y = 0.075*sin(2*pi/10*(i*0.1))
    lVRobXA.append(x)
    lVRobYA.append(y)
    
    #print("x: ",x," y: ",y)
    #"""
    #Carré
    """
    if i%100<25:
        x=0.01
    elif (i%100<75 and i%100>=50):
        x=-0.01
    else:
        x=0
        
    if (i%100>=25 and i%100<50):
        y=0.01
    elif i%100>=75:
        y=-0.01
    else:
        y=0
    """    
    Robot.deplacementEntrainement(Vecteur(x,y),DeltaTemps)
    lDeplRobXA.append(Robot.position.x)
    lDeplRobYA.append(Robot.position.y)
#Robot.environnement.affichageNoeuds()

plt.plot(lDeplRobXA,lDeplRobYA)
#plt.show()
print("Fin Entrainement")

lDeplRobXE = []
lDeplRobYE = []
for j in range (0,1):
    # Random float in [0.0, 1.0)
    x = random.uniform(-2, 2)
    y = random.uniform(-1,3)
    Robot.position = Vecteur(x,y)
    lDeplRobXE.append(Robot.position.x)
    lDeplRobYE.append(Robot.position.y)

    x = random.random()*0.002-0.001
    y = random.random()*0.002-0.001

    Robot.vitesse = Vecteur(x,y)

    #print("PositionInitRobot: ",Robot.position.affichage())
    #print("VitesseInitRobot: ",Robot.vitesse.affichage())

    Robot.environnement.noeuds[-1].poids = -2000

    print("Debut Deplacement")
    j=0
    for i in range(0,1000):        
        Robot.deplacementIDSM(DeltaTemps)
        lDeplRobXE.append(Robot.position.x)
        lDeplRobYE.append(Robot.position.y)
        #print(Robot.position.x," ",Robot.position.y)
    print("Fin Deplacement")
    
Robot.environnement.affichageNoeuds()
print(lDeplRobXE[0],lDeplRobYE[1])
print(len(Robot.environnement.noeuds))
plt.plot(lDeplRobXE,lDeplRobYE)

#print("Attention c'est long")
#lDeplRobXb = []
#lDeplRobYb = []
#for i in range(0,10000):
#    Robot.deplacementIDSM()
#    lDeplRobXb.append(Robot.position.x)
#    lDeplRobYb.append(Robot.position.y)
#plt.plot(lDeplRobXb,lDeplRobYb)

l1=[]
l2=[]
l3=[]
l4=[]

for i in range(-10,10):
    for j in range(-10,15):
        V = Robot.environnement.influenceTt(Vecteur(i*0.2,j*0.2))
        l1.append(i*0.2)
        l2.append(j*0.2)
        l3.append(V.x)
        l4.append(V.y)
plt.quiver(l1,l2,l3,l4)

plt.axes().set_aspect('equal', 'datalim')   

plt.savefig("img.pdf", bbox_inches='tight')    
plt.show()

plt.axes().set_aspect('equal', 'datalim')  

plt.plot(lVRobXA,lVRobYA)
#plt.show()
