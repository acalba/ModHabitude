from Vecteur import *
from Environnement import *
import random

class Robot:
    #Constructeur de Robot
    def __init__(self,p,v):
        self.position = p
        self.vitesse = v
        self.environnement = Environnement()

    #Deplacement selon les noeuds de l'environnement
    def deplacementIDSM(self, DeltaTemps):
        self.environnement.calculPoid(self.position, DeltaTemps)
        self.vitesse = self.environnement.influenceTt(self.position)
        if(self.environnement.calculDensite(self.position)<1):
            noeud = Noeud(self.position,self.vitesse,0)
            self.environnement.ajouterNoeud(noeud)
        self.position = self.position + self.vitesse


    #Deplacement selon le Vecteur v donnée
    def deplacementEntrainement(self, v, DeltaTemps):
        if(self.environnement.calculDensite(self.position)<1):
            noeud = Noeud(self.position,v,0)
            self.environnement.ajouterNoeud(noeud)
        self.environnement.calculPoid(self.position, DeltaTemps)
        self.vitesse= v
        self.position = self.position + self.vitesse

        
"""
robot = Robot(Vecteur(0,0),Vecteur(0.1,0))

for j in range (0,2):
   
    print()
    print("Passage ",j)
    print("Coord1: ",robot.position.affichage())
    print("Vitesse1: ", robot.vitesse.affichage())
    for i in range (0,10):
        robot.deplacement()
        print("Coord2: ",robot.position.affichage())
        print("Vitesse2: ", robot.vitesse.affichage())
    robot.position = Vecteur(0,0)
    robot.vitesse = Vecteur(0,0.1)

for g in range (0,2):
    robot.position = Vecteur(0,0)
    robot.vitesse = Vecteur(0,0)
    print()
    print("Passage ",j+g)
    for i in range (0,10):   
        robot.deplacement()
        print("Coord: ",robot.position.affichage())
        print("Vitesse: ", robot.vitesse.affichage())
    


robot.environnement.affichageNoeuds()"""
