from Vecteur import *
class Noeud:
    kd=1000
    kw=0.0025
    #Constructeur d'un noeud
    def __init__ (self, position, vitesse, poid):
        self.position = position
        self.vitesse = vitesse
        self.poid = poid

    #Calcule le facteur selon la distance entre le noeud et l'entité   
    def distanceFacteur(self, x):
        vecteur = self.position - x
        a= self.kd*vecteur.normal()**2
        if a<709.7:
            return 2/(1+exp(a))
        else:
            return 0.0
        
    #Calcule le facteur selon le poid du noeud
    def poidFacteur(self):
        a= -self.kw*self.poid
        if a<709.7:
            return 2/(1+exp(a))
        else:
            return 0.0

    #Affichage d'un noeud
    def affichage(self):
        return "p: "+self.position.affichage()+" v: "+self.vitesse.affichage()+" w: "+str(self.poid)


"""
#Test de la classe
position = Vecteur(1,1)
vitesse = Vecteur(1,1)
test = Noeud(position,vitesse,1)
print("position: ",test.position.affichage(), " vitesse: ", test.vitesse.affichage(), " poid: ", test.poid)
print("poidFacteur:", test.poidFacteur())
newPosition = Vecteur(1,21.8424)
print("distanceFacteur", test.distanceFacteur(newPosition))"""



