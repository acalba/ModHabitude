from numpy import linalg as LA
from math import *
class Noeud:
    kd=1000
    kw=0.0025
    
    #Constructeur d'un noeud
    def __init__ (self, position, vitesse, poid):
        self.position = position
        self.vitesse = vitesse
        self.poid = poid

    #Calcule le facteur selon la distance entre le noeud et l'entité   
    def distanceFacteur(self, x):
        vecteur = self.position - x
        a= self.kd*(LA.norm(vecteur)**2)
        if a<709.7:
            return 2/(1+exp(a))
        else:
            return 0.0
        
    #Calcule le facteur selon le poid du noeud
    def poidFacteur(self):
        a= -self.kw*self.poid
        if a<709.7:
            return 2/(1+exp(a))
        else:
            return 0.0

    #Affichage d'un noeud
    def affichage(self):
        return "p: "+self.position+" v: "+self.vitesse+" w: "+str(self.poid)
