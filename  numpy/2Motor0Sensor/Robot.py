from Environnement import *
import random

class Robot:
    #Constructeur de Robot
    def __init__(self,p,v):
        self.position = p
        self.vitesse = v
        self.environnement = Environnement()

    def deplacement(self, DeltaTemps):
        self.environnement.calculPoid(self.position, DeltaTemps)
        if(self.environnement.calculDensite(self.position)<1):
            noeud = Noeud(self.position,self.vitesse,0)
            self.environnement.ajouterNoeud(noeud)
        self.position = self.position + self.vitesse*DeltaTemps
        
    #Deplacement selon les noeuds de l'environnement
    def deplacementIDSM(self, DeltaTemps):
        self.vitesse = self.environnement.influenceTt(self.position)
        self.deplacement(DeltaTemps)


    #Deplacement selon le Vecteur v donnée
    def deplacementEntrainement(self, v, DeltaTemps):
        self.vitesse= v
        self.deplacement(DeltaTemps)
