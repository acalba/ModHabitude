from numpy import linalg as LA
from math import *
class Noeud:
    #Constante
    kd=1000
    kw=0.0025
    
    #Constructeur d'un noeud
    def __init__ (self, position, vitesse, sensimotor, acc, poid):
        self.position = position #Position dans l'espace du robot
        self.vitesse = vitesse #Vitesse appliqué à la position dans l'espace
        self.sensimotor = sensimotor #Donnée pour calculer le deplacement du robot
        self.acc = acc #Acceleration
        self.poid = poid #Poid du noeud

    #Calcule le facteur selon la distance entre le noeud et l'entité   
    def distanceFacteur(self, x):
        vecteur = self.position - x
        a= self.kd*(LA.norm(vecteur)**2)
        if a<709.7:
            return 2/(1+exp(a))
        else:
            return 0.0
        
    #Calcule le facteur selon le vecteur sensimotor comprenant la vitesse et l'intensité lumineuse entre le noeud et l'entité   
    def sensimotorFacteur(self, x):
        vecteur = self.sensimotor - x
        a= self.kd*(LA.norm(vecteur)**2)
        if a<709.7:
            return 2/(1+exp(a))
        else:
            return 0.0
        
    #Calcule le facteur selon le poid du noeud
    def poidFacteur(self):
        a= -self.kw*self.poid
        if a<709.7:
            return 2/(1+exp(a))
        else:
            return 0.0

    #Affichage d'un noeud
    def affichage(self):
        return "p: "+self.position.affichage()+" v: "+self.vitesse.affichage()+" w: "+str(self.poid)




