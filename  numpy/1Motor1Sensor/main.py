from Robot import *
from math import *
import random
import matplotlib.pyplot as plt


DeltaTemps= 0.1 #si DeltaTemps trop bas au bout d'un certain temps la courbe se detache
temps = 0
Robot = Robot(np.array([-2.5,0.0]),np.array([(cos(temps/2)/2),0.0]))

normalize_v = lambda v: (v+2.)/4.

#Entrainement du robot
s=1/(1+Robot.position[0]**2)
lDeplX = []
lt = []
lDeplX.append(Robot.position[0])
lt.append(0)
l11=[]
l21=[]
print("Debut entrainement")
while(temps<20):
    temps += DeltaTemps
    x = cos(temps/2)
    Robot.deplacementEntrainement(np.array([x,0.0]),DeltaTemps)
    lDeplX.append(Robot.position[0])
    lt.append(temps)
    l11.append(normalize_v(Robot.sensimotor[0]))
    l21.append(Robot.sensimotor[1])

print("Fin entrainement")

#Premier lacher du robot en partant du dernier point de l'entrainement
lDeplX2 = []
lt2 = []
l12=[]
l22=[]

print("Debut phase IDSM 1")
while(temps<35):
    temps += DeltaTemps
    Robot.deplacementIDSM(DeltaTemps)
    lDeplX2.append(Robot.position[0])
    lt2.append(temps)
    l12.append(normalize_v(Robot.sensimotor[0]))
    l22.append(Robot.sensimotor[1])
    
print("Fin phase IDSM 1")
#Repositionnement du Robot puis deplacement grace aux noeuds
Robot.position = np.array([-2.5,0.0])
Robot.vitesse = np.array([0.0,0.0])
lDeplX3 = []
lt3 = []
l13=[]
l23=[]
print("Debut phase IDSM 2")
while(temps<70):
    temps += DeltaTemps
    Robot.deplacementIDSM(DeltaTemps)
    lDeplX3.append(Robot.position[0])
    lt3.append(temps)
    l13.append(normalize_v(Robot.sensimotor[0]))
    l23.append(Robot.sensimotor[1])
  
print("Fin phase IDSM 2")

plt.plot(lt,lDeplX)
plt.plot(lt2,lDeplX2)
plt.plot(lt3,lDeplX3)

plt.savefig("deplacement.pdf", bbox_inches='tight') 
plt.show()

l1=[]
l2=[]
l3=[]
l4=[]
for n in Robot.environnement.noeuds[1:-1]:
    l1.append(normalize_v(n.sensimotor[0]))
    l2.append(n.sensimotor[1])
    l3.append(n.acc[0])
    l4.append(n.acc[1])
    
plt.quiver(l1,l2,l3,l4)

plt.plot(l11,l21)
plt.plot(l12,l22)
plt.plot(l13,l23)
plt.savefig("motor&light.pdf", bbox_inches='tight') 
plt.show()
