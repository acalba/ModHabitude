from Robot import *
from math import *
import random
import matplotlib.pyplot as plt

from numpy import linalg as LA
from numpy import *
import time
seed = int(time.time())
print(seed)
random.seed(seed)
DeltaTemps= 0.1
temps = 0
lDeplX = []
lDeplY = []

def initRobot(robot):
    x=random.uniform(-2., 2.)
    y=random.uniform(-2., 2.)
    debut = np.array([x,y])
    robot.position = np.array(debut)
    robot.vitesse = np.array([0.0,0.0])
    robot.acc = np.array([0.,0.,0.,0.])
    robot.alpha = random.uniform(-pi, pi)

def saveFigure(type,nb,test,nba):
    axes = plt.gca()
    axes.set_xlim(-2, 2)
    axes.set_ylim(-2, 2)
    if(test == 1):
        nom = "PhotoAxis"
    elif(test == 2):
        nom = "SinPhotoAxis"
    elif(test == 3):
        nom = "PhotoPhobia"
    plt.savefig("image/"+type+str(nb)+nom+str(nba*150)+"-"+str(nba*150+150)+".pdf", bbox_inches='tight')
    plt.figure()       
    #plt.show()

    
def prepGraph(lx,ly,alpha):
    lxr = []
    lyr = []
    plt.quiver(lx[0],ly[0],cos(alpha),sin(alpha))
    lxr.append(lx[0])
    lyr.append(ly[0])
    for i in range(1,len(lx)):
        if((lx[i-1]<-1 and lx[i]>1) or (ly[i-1]<-1 and ly[i]>1) or (lx[i]<-1 and lx[i-1]>1) or (ly[i]<-1 and ly[i-1]>1)):
            if(lx[i-1]<-1 and lx[i]>1):
                if(ly[i-1]<-1 and ly[i]>1):
                    lxr.append(lx[i]-4)
                    lyr.append(ly[i]-4)
                elif (ly[i]<-1 and ly[i-1]>1):
                    lxr.append(lx[i]-4)
                    lyr.append(ly[i]+4)
                else:
                    lxr.append(lx[i]-4)
                    lyr.append(ly[i])
            elif(lx[i]<-1 and lx[i-1]>1):
                if(ly[i-1]<-1 and ly[i]>1):
                    lxr.append(lx[i]+4)
                    lyr.append(ly[i]-4)
                elif (ly[i]<-1 and ly[i-1]>1):
                    lxr.append(lx[i]+4)
                    lyr.append(ly[i]+4)
                else:
                    lxr.append(lx[i]+4)
                    lyr.append(ly[i])
                    
            elif(ly[i-1]<-1 and ly[i]>1):
                lxr.append(lx[i])
                lyr.append(ly[i]-4)
                
            elif(ly[i]<-1 and ly[i-1]>1):
                lxr.append(lx[i])
                lyr.append(ly[i]+4)     
                    
            lxr.append(np.nan)
            lyr.append(np.nan)

            if(lx[i-1]<-1 and lx[i]>1):
                if(ly[i-1]<-1 and ly[i]>1):
                    lxr.append(lx[i-1]+4)
                    lyr.append(ly[i-1]+4)
                elif (ly[i]<-1 and ly[i-1]>1):
                    lxr.append(lx[i-1]+4)
                    lyr.append(ly[i-1]-4)
                else:
                    lxr.append(lx[i-1]+4)
                    lyr.append(ly[i-1])
            elif(lx[i]<-1 and lx[i-1]>1):
                if(ly[i-1]<-1 and ly[i]>1):
                    lxr.append(lx[i-1]-4)
                    lyr.append(ly[i-1]+4)
                elif (ly[i]<-1 and ly[i-1]>1):
                    lxr.append(lx[i-1]-4)
                    lyr.append(ly[i-1]-4)
                else:
                    lxr.append(lx[i-1]-4)
                    lyr.append(ly[i-1])
                    
            elif(ly[i-1]<-1 and ly[i]>1):
                lxr.append(lx[i-1])
                lyr.append(ly[i-1]+4)
                
            elif(ly[i]<-1 and ly[i-1]>1):
                lxr.append(lx[i-1])
                lyr.append(ly[i-1]-4) 

            
        lxr.append(lx[i])
        lyr.append(ly[i])
    plt.plot(lxr,lyr)
"""
robot = Robot(np.array([-1.,-1.]),np.array([0.0,0.0]))
DeltaTemps= 0.1
temps = 0
def ilum(x,y):
    robot.position[0] = x
    robot.position[1] = 0
    #robot.vitesse[0]=0
    robot.alpha = 0
    #print(robot.intensiteLumineuse("gauche"))
    if(y==0):
        return robot.intensiteLumineuse("droite")
    elif(y==1):
        return robot.intensiteLumineuse("gauche")
    robot.deplacementPhototaxis(0.1)
    #robot.deplacementSinPhototaxis(DeltaTemps,temps)
    #robot.deplacementPhotophobia(DeltaTemps)
    return robot.vitesse[0]

xi = np.linspace(-20,0, num=1000)
y = [ilum(x,0) for x in xi]
y1 = [ilum(x,10) for x in xi]
plt.figure()
#plt.plot(xi, y)
plt.plot(xi, y1)
axes = plt.gca()
axes.set_ylim(0, 1)
plt.xlabel('temps',fontsize=24)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.savefig("intensiteLumineuseSelonDistance.pdf", bbox_inches='tight') 
plt.show()
"""

for test in range(1,4):
    robot = Robot(np.array([-1.,-1.]),np.array([0.0,0.0]))
    for k in range (0,10):
        lDeplX = []
        lDeplY = []
        temps = 0
        initRobot(robot)
        alpha = robot.alpha
        print(" Debut: ",robot.position, " e: ", LA.norm(robot.position))
        while(temps<25):
            temps += DeltaTemps
            if(test == 1):
                robot.deplacementPhototaxis(DeltaTemps)
            elif(test == 2):
                robot.deplacementSinPhototaxis(DeltaTemps,temps)
            elif(test == 3):
                robot.deplacementPhotophobia(DeltaTemps)
            lDeplX.append(robot.position[0])
            lDeplY.append(robot.position[1])
        print( " Fin  : ",robot.position, " e: ", LA.norm(robot.position))
        print()
        prepGraph(lDeplX,lDeplY,alpha)
        #plt.gca().add_artist(plt.Circle((robot.position[0], robot.position[1]), 0.1, color='b'))
    saveFigure("Entrainement","2,75",test,-1)

    
    for j in range(0,10):
        lDistance = []
        for i in range(0,3):
            lDeplX = []
            lDeplY = []
            temps = 0
            initRobot(robot)
            alpha = robot.alpha
            robot.sensimotor = np.array([robot.vitesse[0],robot.vitesse[1],robot.intensiteLumineuse("gauche"),robot.intensiteLumineuse("droite")])
            print(i," Debut: ",robot.position, " e: ", LA.norm(robot.position))
            while(temps<50):
                temps += DeltaTemps
                robot.deplacementIDSM(DeltaTemps)
                lDeplX.append(robot.position[0])
                lDeplY.append(robot.position[1])
                lDistance.append(LA.norm(robot.position))
            print(i, " Fin  : ",robot.position, " e: ", LA.norm(robot.position))
            print()
            prepGraph(lDeplX,lDeplY,alpha)
            plt.gca().add_artist(plt.Circle((robot.position[0], robot.position[1]), 0.1, color='k'))
        print("Moyenne: ",np.mean(lDistance))
        saveFigure("IDSM","2,75",test,j) 
    plt.close('all')

