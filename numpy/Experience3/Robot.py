from Environnement import *
from numpy import linalg as LA
import numpy as np
import random

class Robot:
    
    #Constructeur de Robot
    def __init__(self,p,v):
        self.r = 0.25
        self.beta = pi/3.0
        self.position = p
        self.vitesse = v
        self.environnement = Environnement()
        self.acc = np.array([0.,0.,0.,0.])
        self.alpha = 0.0
        self.sensimotor = np.array([self.vitesse[0],self.vitesse[1],self.intensiteLumineuse("gauche"),self.intensiteLumineuse("droite")])
        

    #Actualise le poid des noeuds puis change la position du robot selon la vitesse de ce dernier
    def deplacement (self,DeltaTemps, time):
        if(self.environnement.calculDensite(self.sensimotor)<1):
            noeud = Noeud(self.position,self.vitesse,self.sensimotor,self.acc, 0, time)
            self.environnement.ajouterNoeud(noeud)
        self.environnement.calculPoid(self.sensimotor,DeltaTemps)
        self.position[0] += cos(self.alpha)*(self.vitesse[0]+self.vitesse[1])*DeltaTemps
        self.position[1] += sin(self.alpha)*(self.vitesse[0]+self.vitesse[1])*DeltaTemps
        taille = 2
        if(self.position[0]>taille):
            self.position[0] -= 2*taille
        elif(self.position[0]<- taille):
            self.position[0] += 2* taille
        if(self.position[1]>taille):
            self.position[1] -= 2* taille
        elif(self.position[1]<-taille):
            self.position[1] += 2*taille

        self.alpha += 2*(self.vitesse[1]-self.vitesse[0])*DeltaTemps
        self.alpha = self.alpha%(2*pi)
        #Calcul de sensimotor servant au deplacement IDSM
        self.sensimotor = np.array([self.vitesse[0],self.vitesse[1],self.intensiteLumineuse("gauche"),self.intensiteLumineuse("droite")])

        
    #Deplacement selon les noeuds de l'environnement
    def deplacementIDSM(self,DeltaTemps):
        #Calcul de l'acceleration selon les noeuds 
        self.acc = self.environnement.influenceTt(self.sensimotor)
        self.vitesse[0] += self.acc[0]
        self.vitesse[1] += self.acc[1]
        self.deplacement(DeltaTemps, 0)

    #Deplacement attirer par la lumière 2.7
    def deplacementPhototaxis(self,DeltaTemps):
        #z = 2.75
        z = 2.55
        Xl = 1-(z*sqrt(self.intensiteLumineuse("gauche")))
        Xr = 1-(z*sqrt(self.intensiteLumineuse("droite")))
        #print("(x,y,alpha) = ({},{},{}) ; Xl: {} , sl = {} ; Xr = {}, sr = {}".format(self.position[0],self.position[1], self.alpha ,Xl, self.intensiteLumineuse("gauche"),Xr,self.intensiteLumineuse("droite")))
        self.deplacementEntrainement(Xl, Xr, DeltaTemps)
        
     #Deplacement attirer par la lumière
    def deplacementSinPhototaxis(self,DeltaTemps,t):
        z = 2.75
        #z = 1.5
        Xl = 1-(z*sqrt(self.intensiteLumineuse("gauche")))+sin(2*t)/2
        Xr = 1-(z*sqrt(self.intensiteLumineuse("droite")))-sin(2*t)/2
        self.deplacementEntrainement(Xl, Xr, DeltaTemps)
    
      #Deplacement attirer par la lumière
    def deplacementPhotophobia(self,DeltaTemps):
        z = 2.75
        #z = 1.5
        Xr = 1-(z*sqrt(self.intensiteLumineuse("gauche")))
        Xl = 1-(z*sqrt(self.intensiteLumineuse("droite")))
        self.deplacementEntrainement(Xl, Xr, DeltaTemps)
        
    #Deplacement selon le Vecteur v donnée
    def deplacementEntrainement(self, Xl, Xr, DeltaTemps):
        self.acc = np.array([(Xl-self.vitesse[0]),(Xr-self.vitesse[1]),(self.intensiteLumineuse("gauche")-self.acc[2]),(self.intensiteLumineuse("droite")-self.acc[3])])
        self.vitesse[0] += self.acc[0]
        self.vitesse[1] += self.acc[1]
        self.deplacement(DeltaTemps, 11)
        
    def intensiteLumineuse(self, cote):
        if cote == "droite":
            beta = -self.beta
        else:
            beta = self.beta
        b = np.array([cos(self.alpha+beta),sin(self.alpha+beta)])
        e = np.array([-(self.position[0]+self.r*cos(self.alpha+beta)),-(self.position[1]+self.r*sin(self.alpha+beta))])
        c = e / LA.norm(e)
        d = LA.norm(e)
        haut = np.dot(b,c)
        if(haut< 0):
            haut=0
        #print("haut : {} , d : {}".format(haut, d))
        s=haut/(1+(d**2))
        return s
 
        
    
