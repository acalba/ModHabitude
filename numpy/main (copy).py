from math import *
import random
import matplotlib.pyplot as plt

from numpy import linalg as LA
import numpy as np
import time


def ilum(x):
    return 2/(1+exp(1000*x*x))

xi = np.linspace(0,0.15, num=1000)
y = [ilum(x) for x in xi]
plt.figure()
plt.plot(xi, y)
plt.xlabel('||Np-x|| (Distance du noeud)',fontsize=24)
plt.ylabel('d(Np,x)',fontsize=24)
plt.title('Facteur distance',fontsize=32)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
axes = plt.gca()
plt.savefig("FacteurDistance.pdf", bbox_inches='tight') 
plt.show()
