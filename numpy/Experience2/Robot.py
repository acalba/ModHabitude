from Environnement import *
from numpy import linalg as LA
import numpy as np
import random

class Robot:
    #Constructeur de Robot
    def __init__(self,p,v):
        self.position = p
        self.vitesse = v
        self.environnement = Environnement()
        self.sensimotor =  np.array([v[0],1/(1+self.position[0]**2)])
        self.acc = np.array([0.,0.])

    #Actualise le poid des noeuds puis change la position du robot selon la vitesse de ce dernier
    def deplacement (self,DeltaTemps, time):
        if(self.environnement.calculDensite(self.sensimotor)<1):
            noeud = Noeud(self.sensimotor, self.acc, 0, time)
            self.environnement.ajouterNoeud(noeud)
        self.environnement.calculPoid(self.sensimotor,DeltaTemps)
        self.position +=   self.vitesse*DeltaTemps
        #Calcul de sensimotor servant au deplacement IDSM
        self.sensimotor = np.array([self.vitesse[0],1/(1+self.position[0]**2)])
        
    #Deplacement selon les noeuds de l'environnement
    def deplacementIDSM(self,DeltaTemps):
        #Calcul de l'acceleration selon les noeuds 
        self.acc = self.environnement.influenceTt(self.sensimotor)
        self.vitesse += self.acc[0]*DeltaTemps
        self.deplacement(DeltaTemps, 0)

    #Deplacement selon le Vecteur v donnée
    def deplacementEntrainement(self, v, DeltaTemps):
        #Calcul de l'acceleration difference vitesse actuel vitesse  
        self.acc = (v - self.vitesse)/DeltaTemps
        self.vitesse = v
        self.deplacement(DeltaTemps, 11)
