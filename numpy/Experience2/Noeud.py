from numpy import linalg as LA
from math import *
class Noeud:
    #Constante
    kd=1000
    kw=0.0025
    
    #Constructeur d'un noeud
    def __init__ (self, sensimotor, acc, poid, time):
        self.sensimotor = sensimotor #Donnée pour calculer le deplacement du robot
        self.acc = acc #Acceleration
        self.poid = poid #Poid du noeud
        self.time = time
        
    #Calcule le facteur selon le vecteur sensimotor comprenant la vitesse et l'intensité lumineuse entre le noeud et l'entité   
    def sensimotorFacteur(self, x):
        vecteur = self.sensimotor - x
        a= self.kd*(LA.norm(vecteur)**2)
        if a<709.7:
            return 2/(1+exp(a))
        else:
            return 0.0
        
    #Calcule le facteur selon le poid du noeud
    def poidFacteur(self):
        a= -self.kw*self.poid
        if a<709.7:
            return 2/(1+exp(a))
        else:
            print("poidfacteur=0")
            return 0.0

    #Affichage d'un noeud
    def affichage(self):
        print("p: ",self.sensimotor," acc: ",self.acc ," w: ",self.poid)




