from math import *
import random
import matplotlib.pyplot as plt

from numpy import linalg as LA
import numpy as np
import time


def ilum(x):
    return 2/(1+exp(-0.0025*x))

xi = np.linspace(-2000,2000, num=1000)
y = [ilum(x) for x in xi]
plt.figure()
plt.plot(xi, y)
plt.xlabel('N$\omega$ (Poid du noeud)',fontsize=24)
plt.ylabel(r'$\omega$(N$\omega$)',fontsize=24)
plt.title('Facteur poid',fontsize=32)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
axes = plt.gca()
plt.savefig("FacteurPoid.pdf", bbox_inches='tight') 
plt.show()
