from Robot import *
from math import *
import random
import matplotlib.pyplot as plt
import numpy as np
import time
seed = int(time.time())
print(seed)
random.seed(seed)

DeltaTemps = 1
temps = 0

print("Debut Entrainement")
Robot = Robot(np.array([0,-1.25]),np.array([0.0001,0.0001]))
lDeplRobXA = []
lDeplRobYA = []
lVRobXA = []
lVRobYA = []
x=0.1
y=0.1
lDeplRobXA.append(Robot.position[0])
lDeplRobYA.append(Robot.position[1])
for i in range(0,100):
    #Cercle
    #"""
    x = 0.075*cos(2*pi/10*(i*0.1))
    y = 0.075*sin(2*pi/10*(i*0.1))
    lVRobXA.append(x)
    lVRobYA.append(y)
    Robot.deplacementEntrainement(np.array([x,y]),DeltaTemps)
    lDeplRobXA.append(Robot.position[0])
    lDeplRobYA.append(Robot.position[1])
#Robot.environnement.affichageNoeuds()

plt.plot(lDeplRobXA,lDeplRobYA)
#plt.show()
print("Fin Entrainement")

lDeplRobXE = []
lDeplRobYE = []
# Random float in [0.0, 1.0)
x = random.uniform(-1.5, 1.5)
y = random.uniform(-1.5, 1.5)
Robot.position = np.array([x,y])
lDeplRobXE.append(Robot.position[0])
lDeplRobYE.append(Robot.position[1])

x = 0.
y = 0.
Robot.vitesse = np.array([x,y])

print("Debut Deplacement")
j=0
for i in range(0,200):        
    Robot.deplacementIDSM(DeltaTemps)
    lDeplRobXE.append(Robot.position[0])
    lDeplRobYE.append(Robot.position[1])
print("Fin Deplacement")
    
#Robot.environnement.affichageNoeuds()
print(lDeplRobXE[0],lDeplRobYE[1])
print(len(Robot.environnement.noeuds))
plt.plot(lDeplRobXE,lDeplRobYE)

#print("Attention c'est long")
#lDeplRobXb = []
#lDeplRobYb = []
#for i in range(0,10000):
#    Robot.deplacementIDSM()
#    lDeplRobXb.append(Robot.position.x)
#    lDeplRobYb.append(Robot.position.y)
#plt.plot(lDeplRobXb,lDeplRobYb)

l1=[]
l2=[]
l3=[]
l4=[]

for i in range(-10,11):
    for j in range(-10,10):
        V = Robot.environnement.influenceTt(np.array([i*0.2,j*0.2]))
        l1.append(i*0.2)
        l2.append(j*0.2)
        l3.append(V[0])
        l4.append(V[1])
plt.quiver(l1,l2,l3,l4)

plt.axes().set_aspect('equal', 'datalim')

plt.xlabel('Moteur 1',fontsize=16)
plt.ylabel('Moteur 2',fontsize=16)
plt.title('Espace Sensorimoteur',fontsize=24)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)

plt.savefig("img.pdf", bbox_inches='tight')    
plt.show()

plt.axes().set_aspect('equal', 'datalim')  

plt.plot(lVRobXA,lVRobYA)
#plt.show()
