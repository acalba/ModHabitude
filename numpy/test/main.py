from Robot import *
from math import *
import random
import matplotlib.pyplot as plt
from numpy import linalg as LA

seed = random.random()
random.seed(seed)
DeltaTemps= 0.1 
x1 = random.uniform(-2., 2.)
x2 = random.uniform(-2., 2.)
robot = Robot(np.array([x1,x2]),np.array([0.0,0.0]))
for i in range (0,100):
    print("i: ",i, " n: ",len(robot.environnement.noeuds))
    for j in range (0,50):
        x1=random.uniform(-0.05, 0.05)
        x2=random.uniform(-0.05, 0.05)
        r = np.array([x1,x2,0.0,0.0])
        robot.deplacementEntrainement(r,DeltaTemps)
print(len(robot.environnement.noeuds))
print("debut")
for i in range(0,4):
    lDeplX = []
    lDeplY = []
    temps = 0
    x=random.uniform(-2., 2.)
    y=random.uniform(-2., 2.)
    debut = np.array([x,y])
    robot.position = np.array(debut)
    x1=random.uniform(-2., 2.)
    x2=random.uniform(-2., 2.)
    r = np.array([x1,x2,0.0,0.0])
    robot.sensimotor = r
    while(temps<300):
        print("i: ", i," t: ",temps)
        temps += DeltaTemps
        robot.deplacementIDSM(DeltaTemps)

        lDeplX.append(robot.position[0])
        lDeplY.append(robot.position[1])
    plt.plot(lDeplX,lDeplY)
plt.savefig("TestDeplacementIDSM"+str(seed)+".pdf", bbox_inches='tight')
print(seed)
plt.show()
