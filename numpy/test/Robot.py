from Environnement import *
from numpy import linalg as LA
import numpy as np
import random

class Robot:
    
    #Constructeur de Robot
    def __init__(self,p,v):
        self.position = p
        self.vitesse = v
        self.environnement = Environnement()
        self.acc = np.array([0.,0.,0.,0.])
        self.alpha = 0.0
        self.sensimotor = np.array([self.vitesse[0],self.vitesse[1],self.intensiteLunimeuse("gauche"),self.intensiteLunimeuse("droite")])
        

    #Actualise le poid des noeuds puis change la position du robot selon la vitesse de ce dernier
    def deplacement (self,DeltaTemps, time):
        if(self.environnement.calculDensite(self.sensimotor)<1):
            noeud = Noeud(self.sensimotor,self.acc,0, time)
            self.environnement.ajouterNoeud(noeud)
        self.environnement.calculPoid(self.sensimotor,DeltaTemps)
        self.position[0] += cos(self.alpha)*(self.vitesse[0]+self.vitesse[1])*DeltaTemps
        self.position[1] += sin(self.alpha)*(self.vitesse[0]+self.vitesse[1])*DeltaTemps
        self.alpha += 2*(self.vitesse[1]-self.vitesse[0])*DeltaTemps
        self.alpha = self.alpha%(2*pi)
        #Calcul de sensimotor servant au deplacement IDSM
        self.sensimotor = np.array([self.vitesse[0],self.vitesse[1],self.intensiteLunimeuse("gauche"),self.intensiteLunimeuse("droite")])
        
    #Deplacement selon les noeuds de l'environnement
    def deplacementIDSM(self,DeltaTemps):
        #Calcul de l'acceleration selon les noeuds 
        self.acc = self.environnement.influenceTt(self.sensimotor)
        self.acc[2] = 0.0
        self.acc[3] = 0.0
        self.vitesse[0] += self.acc[0]
        self.vitesse[1] += self.acc[1]
        self.deplacement(DeltaTemps, 0)
        

    #Deplacement attirer par la lumière
    def deplacementPhototaxis(self,DeltaTemps):
        Xl = 1-(1.5*sqrt(self.intensiteLunimeuse("gauche")))
        Xr = 1-(1.5*sqrt(self.intensiteLunimeuse("droite")))
        self.deplacementEntrainementA(Xl, Xr, DeltaTemps)
        
     #Deplacement attirer par la lumière
    def deplacementSinPhototaxis(self,DeltaTemps,t):
        Xl = 1-(1.5*sqrt(self.intensiteLunimeuse("gauche")))+sin(2*t)/2
        Xr = 1-(1.5*sqrt(self.intensiteLunimeuse("droite")))-sin(2*t)/2
        self.deplacementEntrainementA(Xl, Xr, DeltaTemps)
        
      #Deplacement attirer par la lumière
    def deplacementPhotophobia(self,DeltaTemps):
        Xr = 1-(1.5*sqrt(self.intensiteLunimeuse("gauche")))
        Xl = 1-(1.5*sqrt(self.intensiteLunimeuse("droite")))
        self.deplacementEntrainementA(Xl, Xr, DeltaTemps)
        
    def deplacementEntrainementA(self, Xl, Xr, DeltaTemps):
        self.acc = np.array([(Xl-self.vitesse[0]),(Xr-self.vitesse[1]),0.,0.])
        self.vitesse[0] += self.acc[0]
        self.vitesse[1] += self.acc[1]
        self.deplacement(DeltaTemps, 11)
        #self.sensimotor = np.array([self.vitesse[0],self.vitesse[1],self.intensiteLunimeuse("gauche"),self.intensiteLunimeuse("droite")])
        
    def deplacementEntrainement(self,v, DeltaTemps):
        self.acc = np.array([v[0]-self.vitesse[0],v[1]-self.vitesse[1],0.,0.])
        self.vitesse[0] = v[0]
        self.vitesse[1] = v[1]
        noeud = Noeud(v,(v-self.sensimotor), 0, 11)
        self.environnement.ajouterNoeud(noeud)
        #self.sensimotor = v
        self.deplacement(DeltaTemps, 11)
        
    def intensiteLunimeuse(self, cote):
        r = 0.25
        beta = -pi/3
        if cote == "droite":
            beta = -beta
        b = np.array([cos(self.alpha+beta),sin(self.alpha+beta)])
        #A=position B=[0,0] (xB−xA;yB−yA)
        e = np.array([self.position[0]+r*cos(self.alpha+beta),self.position[1]+r*sin(self.alpha+beta)])
        c = e/LA.norm(e)
        d = LA.norm(e)
        haut = np.dot(b,c)
        if(haut< 0):
            haut=0
        s=haut/(1+(d**2))
        return s
 
        
