from Robot import *
from math import *
import random
import matplotlib.pyplot as plt
from numpy import linalg as LA
#from sklearn.decomposition import PCA
import time
def prepGraph(lx,ly):
    lxr = []
    lyr = []
    lxr.append(lx[0])
    lyr.append(ly[0])
    for i in range(1,len(lx)):
        if((lx[i-1]<-1 and lx[i]>1) or (ly[i-1]<-1 and ly[i]>1) or (lx[i]<-1 and lx[i-1]>1) or (ly[i]<-1 and ly[i-1]>1)):
            if(lx[i-1]<-1 and lx[i]>1):
                if(ly[i-1]<-1 and ly[i]>1):
                    lxr.append(lx[i]-4)
                    lyr.append(ly[i]-4)
                elif (ly[i]<-1 and ly[i-1]>1):
                    lxr.append(lx[i]-4)
                    lyr.append(ly[i]+4)
                else:
                    lxr.append(lx[i]-4)
                    lyr.append(ly[i])
            elif(lx[i]<-1 and lx[i-1]>1):
                if(ly[i-1]<-1 and ly[i]>1):
                    lxr.append(lx[i]+4)
                    lyr.append(ly[i]-4)
                elif (ly[i]<-1 and ly[i-1]>1):
                    lxr.append(lx[i]+4)
                    lyr.append(ly[i]+4)
                else:
                    lxr.append(lx[i]+4)
                    lyr.append(ly[i])
                    
            elif(ly[i-1]<-1 and ly[i]>1):
                lxr.append(lx[i])
                lyr.append(ly[i]-4)
                
            elif(ly[i]<-1 and ly[i-1]>1):
                lxr.append(lx[i])
                lyr.append(ly[i]+4)     
                    
            lxr.append(np.nan)
            lyr.append(np.nan)

            if(lx[i-1]<-1 and lx[i]>1):
                if(ly[i-1]<-1 and ly[i]>1):
                    lxr.append(lx[i-1]+4)
                    lyr.append(ly[i-1]+4)
                elif (ly[i]<-1 and ly[i-1]>1):
                    lxr.append(lx[i-1]+4)
                    lyr.append(ly[i-1]-4)
                else:
                    lxr.append(lx[i-1]+4)
                    lyr.append(ly[i-1])
            elif(lx[i]<-1 and lx[i-1]>1):
                if(ly[i-1]<-1 and ly[i]>1):
                    lxr.append(lx[i-1]-4)
                    lyr.append(ly[i-1]+4)
                elif (ly[i]<-1 and ly[i-1]>1):
                    lxr.append(lx[i-1]-4)
                    lyr.append(ly[i-1]-4)
                else:
                    lxr.append(lx[i-1]-4)
                    lyr.append(ly[i-1])
                    
            elif(ly[i-1]<-1 and ly[i]>1):
                lxr.append(lx[i-1])
                lyr.append(ly[i-1]+4)
                
            elif(ly[i]<-1 and ly[i-1]>1):
                lxr.append(lx[i-1])
                lyr.append(ly[i-1]-4) 

            
        lxr.append(lx[i])
        lyr.append(ly[i])
    plt.plot(lxr,lyr)
    
def initRobot(robot):
    x=random.uniform(-2., 2.)
    y=random.uniform(-2., 2.)
    debut = np.array([x,y])
    robot.position = np.array(debut)
    x1 = random.uniform(-1., 1.)
    x2 = random.uniform(-1., 1.)
    x3 = random.uniform(0., 1.)
    x4 = random.uniform(0., 1.)
    r = np.array([x1,x2,x3,x4])
    robot.sensimotor = r


for i in range (0,1000):
    lDeplX = []
    lDeplY = []
    seed = int(time.time())
    random.seed(seed)
    DeltaTemps= 0.1 
    robot = Robot(np.array([0.,0.]),np.array([0.0,0.0]))
    for i in range (0,100):
        initRobot(robot)
        print("i: ",i, " n: ",len(robot.environnement.noeuds))
        for j in range (0,50):
            x1=random.uniform(-0.05, 0.05)
            x2=random.uniform(-0.05, 0.05)
            x3=random.uniform(-0.05, 0.05)
            x4=random.uniform(-0.05, 0.05)
            r = np.array([x1,x2,x3,x4])
            robot.deplacementEntrainement(r,DeltaTemps)
    #plt.show()
    print(len(robot.environnement.noeuds))
    print("debut")


    for i in range(0,1):
        lDeplX = []
        lDeplY = []
        temps = 0
        initRobot(robot)
        while(temps<100):
            print("i: ", i," t: ",temps)
            temps += DeltaTemps
            robot.deplacementIDSM(DeltaTemps)
            if temps>75:
                lDeplX.append(robot.position[0])
                lDeplY.append(robot.position[1])
    prepGraph(lDeplX,lDeplY)
    plt.savefig("DeplacementIDSM"+str(seed)+".pdf", bbox_inches='tight')
    plt.figure()

    axes = plt.gca()
    axes.set_xlim(-2, 2)
    axes.set_ylim(-2, 2)
    prepGraph(lDeplX,lDeplY)
    plt.savefig("DeplacementIDSM"+str(seed)+"Norm.pdf", bbox_inches='tight')
    plt.figure()
    """
    pca = PCA(n_components=2)
    tabPca = pca.fit_transform(np.array(lDeplX,lDeplY))
    plt.plot(tabPca[1],tabPca[2])
    plt.savefig("PCA"+str(seed)+".pdf", bbox_inches='tight')
    plt.figure()
    """
    plt.close('all')
