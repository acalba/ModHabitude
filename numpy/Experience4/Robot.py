from Environnement import *
from numpy import linalg as LA
import numpy as np
import random

class Robot:
    
    #Constructeur de Robot
    def __init__(self,p,v):
        self.r = 0.25
        self.beta = pi/3.0
        self.position = p
        self.vitesse = v
        self.environnement = Environnement()
        self.acc = np.array([0.,0.,0.,0.])
        self.alpha = 0.0
        self.sensimotor = np.array([self.vitesse[0],self.vitesse[1],self.intensiteLumineuse("gauche"),self.intensiteLumineuse("droite")])
        
    #Actualise le poid des noeuds puis change la position du robot selon la vitesse de ce dernier
    def deplacement (self,DeltaTemps, time, t):
        if(self.environnement.calculDensite(self.sensimotor)<t):
            noeud = Noeud(self.sensimotor,self.acc, 0, time)
            self.environnement.ajouterNoeud(noeud)
        self.environnement.calculPoid(self.sensimotor,DeltaTemps)
        self.position[0] += cos(self.alpha)*(self.vitesse[0]+self.vitesse[1])*DeltaTemps
        self.position[1] += sin(self.alpha)*(self.vitesse[0]+self.vitesse[1])*DeltaTemps
        self.alpha += 2*(self.vitesse[1]-self.vitesse[0])*DeltaTemps
        self.alpha = self.alpha%(2*pi)
        if(t==1):
            taille = 2
            if(self.position[0]>taille):
                self.position[0] -= 2*taille
            elif(self.position[0]<- taille):
                self.position[0] += 2* taille
            if(self.position[1]>taille):
                self.position[1] -= 2* taille
            elif(self.position[1]<-taille):
                self.position[1] += 2*taille

    #Deplacement selon les noeuds de l'environnement
    def deplacementIDSM(self,DeltaTemps):
        #Calcul de l'acceleration selon les noeuds 
        self.acc = self.environnement.influenceTt(self.sensimotor)
        self.vitesse[0] += self.acc[0]
        self.vitesse[1] += self.acc[1]
        self.sensimotor =  np.array([self.vitesse[0],self.vitesse[1],self.intensiteLumineuse("gauche"),self.intensiteLumineuse("gauche")])
        self.deplacement(DeltaTemps, 0,1)
  
        #Deplacement selon le Vecteur v donnée
    def deplacementEntrainement(self,r, DeltaTemps):
        self.acc = r
        self.vitesse[0] = self.acc[0]
        self.vitesse[1] = self.acc[1]
        px = self.position[0] + cos(self.alpha)*(self.vitesse[0]+self.vitesse[1])*DeltaTemps
        py = self.position[1] + sin(self.alpha)*(self.vitesse[0]+self.vitesse[1])*DeltaTemps
        if (px < -2):
            self.vitesse[0] -= (self.acc[0]*2)
        elif (px > 2):
            self.vitesse[0] -= (self.acc[0]*2)
        if (py < -2):
            self.vitesse[1] -= (self.acc[1]*2)
        elif (py > 2):
            self.vitesse[1] -= (self.acc[1]*2)
        self.sensimotor = np.array([self.vitesse[0],self.vitesse[1],r[2]+self.sensimotor[2],r[3]+self.sensimotor[3]])
        if (self.sensimotor[2] < 0):
            self.sensimotor[2] -= (r[2]*2)
        elif (self.sensimotor[2] > 1):
            self.sensimotor[2] -= (r[2]*2)
        if (self.sensimotor[3] < 0):
            self.sensimotor[3] -= (r[3]*2)
        elif (self.sensimotor[3] > 1):
            self.sensimotor[3] -= (r[3]*2)
        self.deplacement(DeltaTemps, 11, 10000000)
        
    def intensiteLumineuse(self, cote):
        if cote == "droite":
            beta = -self.beta
        else:
            beta = self.beta
        b = np.array([cos(self.alpha+beta),sin(self.alpha+beta)])
        e = np.array([-(self.position[0]+self.r*cos(self.alpha+beta)),-(self.position[1]+self.r*sin(self.alpha+beta))])
        c = e / LA.norm(e)
        d = LA.norm(e)
        haut = np.dot(b,c)
        if(haut < 0):
            haut = 0
        s = haut/(1+(d**2))
        return s
 
        
    
