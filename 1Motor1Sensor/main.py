from Robot import *
from math import *
import random
import matplotlib.pyplot as plt


DeltaTemps= 0.1 #si DeltaTemps trop bas au bout d'un certain temps la courbe se detache
Robot = Robot(Vecteur(-2.5,0),Vecteur((cos(-1*DeltaTemps/2)/2),0))


#Entrainement du robot
s=1/(1+Robot.position.x**2)
y=0
lDeplX = []
lt = []
lDeplX.append(Robot.position.x)
lt.append(0)

for i in range (1,int(31*(1/DeltaTemps))):
    t = i*DeltaTemps
    x = cos(t/2)/2
    Robot.deplacementEntrainement(Vecteur(x,0),DeltaTemps)
    lDeplX.append(Robot.position.x)
    lt.append(t)


l11=[]
l21=[]
cpt = 0
for n in Robot.environnement.noeuds:
    l11.append((1+n.sensimotor.x)/2)
    l21.append(n.sensimotor.y)
    cpt+=1
l11.append((1+Robot.environnement.noeuds[0].sensimotor.x)/2)
l21.append(Robot.environnement.noeuds[0].sensimotor.y)



#Premier lacher du robot en partant du dernier point de l'entrainement
lDeplX2 = []
lt2 = []
for j in range (i+1,i+int(30*(1/DeltaTemps))):
    Robot.deplacementIDSM(DeltaTemps)
    lDeplX2.append(Robot.position.x)
    lt2.append(j*DeltaTemps)

l12=[]
l22=[]
cpt2 = 0
for n in Robot.environnement.noeuds:
    if(cpt2>=cpt):
        l12.append((1+n.sensimotor.x)/2)
        l22.append(n.sensimotor.y)
    cpt2+=1


    
#Repositionnement du Robot puis deplacement grace aux noeuds
Robot.position = Vecteur(-2.5,0)
Robot.vitesse = Vecteur(0,0)
lDeplX3 = []
lt3 = []
for k in range (j+1,j+int(70*(1/DeltaTemps))):
    Robot.deplacementIDSM(DeltaTemps)
    lDeplX3.append(Robot.position.x)
    lt3.append(k*DeltaTemps)
    
plt.plot(lt,lDeplX)
plt.plot(lt2,lDeplX2)
plt.plot(lt3,lDeplX3)

plt.savefig("deplacement.pdf", bbox_inches='tight') 
plt.show()
l13=[]
l23=[]
cpt3 = 0
for n in Robot.environnement.noeuds:
    if(cpt3>=cpt2):
        l13.append((1+n.sensimotor.x)/2)
        l23.append(n.sensimotor.y)
    cpt3+=1

#Affichage champs vectoriel sur motor&light   
"""
lx = []
ly = []
coordx = []
coordy = []
for i in range (0,10):
    for j in range (0,10):
        acc = Robot.environnement.influenceTt(Vecteur(i*0.1,j*0.1))
        lx.append(acc.x)
        ly.append(acc.y)
        coordx.append(i*0.1)
        coordy.append(j*0.1)
        
plt.quiver(coordx,coordy,lx,ly,10)       
"""

l1=[]
l2=[]
l3=[]
l4=[]
for n in Robot.environnement.noeuds:
    l1.append((1+n.sensimotor.x)/2)
    l2.append(n.sensimotor.y)
    l3.append(n.acc.x)
    l4.append(n.acc.y)
    
plt.quiver(l1,l2,l3,l4)
plt.plot(l11,l21)
plt.plot(l12,l22)
plt.plot(l13,l23)
plt.savefig("motor&light.pdf", bbox_inches='tight') 
plt.show()
