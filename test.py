from Robot import *
from math import *
import random
import matplotlib.pyplot as plt

print("Debut Entrainement")
Robot = Robot(Vecteur(0,0),Vecteur(0.001,0.001))
lDeplRobXA = []
lDeplRobYA = []
x=0.1
y=0.1
lDeplRobXA.append(Robot.position.x)
lDeplRobYA.append(Robot.position.y)
for i in range(0,1000):
    #Cercle
    #"""
    x = 0.0075*cos(2*pi/10*(i*0.1))
    y = 0.0075*sin(2*pi/10*(i*0.1))
    #"""
    #Carré
    """
    if i%100<25:
        x=0.01
    elif (i%100<75 and i%100>=50):
        x=-0.01
    else:
        x=0
        
    if (i%100>=25 and i%100<50):
        y=0.01
    elif i%100>=75:
        y=-0.01
    else:
        y=0
    """    
    Robot.deplacementEntrainement(Vecteur(x,y))
    lDeplRobXA.append(Robot.position.x)
    lDeplRobYA.append(Robot.position.y)
#Robot.environnement.affichageNoeuds()

plt.plot(lDeplRobXA,lDeplRobYA)
#plt.show()
print("Fin Entrainement")

lDeplRobXE = []
lDeplRobYE = []
# Random float in [0.0, 1.0)
x = random.random()*0.2-0.1
y = random.random()*0.2-0.1
Robot.position = Vecteur(x,y)
lDeplRobXE.append(Robot.position.x)
lDeplRobYE.append(Robot.position.y)

x = random.random()*0.002-0.001
y = random.random()*0.002-0.001

Robot.vitesse = Vecteur(x,y)

#print("PositionInitRobot: ",Robot.position.affichage())
#print("VitesseInitRobot: ",Robot.vitesse.affichage())

Robot.environnement.noeuds[-1].poids = -2000

print("Debut Deplacement")
j=0
for i in range(0,2000):
    if j/20>=1:
        print(i/20,"%")
        j=1
    else:
        j+=1
        
    Robot.deplacementIDSM()
    lDeplRobXE.append(Robot.position.x)
    lDeplRobYE.append(Robot.position.y)
    #print(Robot.position.x," ",Robot.position.y)
print("Fin Deplacement")
Robot.environnement.affichageNoeuds()
#print(lDeplRobXE[0],lDeplRobYE[1])
print(len(Robot.environnement.noeuds))
plt.plot(lDeplRobXE,lDeplRobYE)

#print("Attention c'est long")
#lDeplRobXb = []
#lDeplRobYb = []
#for i in range(0,10000):
#    Robot.deplacementIDSM()
#   lDeplRobXb.append(Robot.position.x)
#    lDeplRobYb.append(Robot.position.y)
#plt.plot(lDeplRobXb,lDeplRobYb)
for n in Robot.environnement.noeuds: 
    plt.quiver(n.position.x,n.position.y,n.vitesse.x,n.vitesse.y)
        
plt.show()
