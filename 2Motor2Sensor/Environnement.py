from Noeud import *

class Environnement:
    #Constructeur de l'environnement
    def __init__(self):
        self.noeuds = []

    #Ajoute le noeud en parametre dans la liste des noeuds
    def ajouterNoeud(self,noeud):
        self.noeuds.append(noeud)

    #Calcul la densite des noeuds autour de l'entité
    def calculDensite(self,x):
        rep = 0;
        for n in self.noeuds:
            rep += n.poidFacteur() * n.sensimotorFacteur(x)
        return rep
    
    #Calcul l'influence des noeuds sur l'entité
    def influenceTt(self,x):
        rep = Vecteur(0,0);
        if(self.calculDensite(x)==0):
            return rep
        for n in self.noeuds:
            a = n.sensimotor-x
            r= a*(n.acc.vecteurUnitaire())*(n.acc.vecteurUnitaire())
            r2 = a-r
            rep += (n.poidFacteur() * n.sensimotorFacteur(x) * (n.acc+r2))
        rep = (1/self.calculDensite(x))*rep
        #pour n'avoir l'acceleration que sur un seul axe car une seul roue
        rep = Vecteur(rep.x,0)
        return rep

    
    #Change le poid des noeuds selon le placement de l'entité
    def calculPoid(self,x):
        for n in self.noeuds:
            r = 10*n.sensimotorFacteur(x)
            n.poid += (-1+r)

    #Affichage des noeuds 
    def affichageNoeuds(self):
        for n in self.noeuds:
            print(n.affichage())
