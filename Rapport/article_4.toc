\select@language {french}
\contentsline {section}{\numberline {1}L'entreprise}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Le Loria}{4}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Un demi-si\IeC {\`e}cle d\IeC {\textquoteright }histoire\IeC {\textellipsis }}{4}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Cr\IeC {\'e}ation du LORIA}{5}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Les diff\IeC {\'e}rents secteur du LORIA}{5}{subsubsection.1.1.3}
\contentsline {subsection}{\numberline {1.2}L'\IeC {\'e}quipe biscuit}{6}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Principe}{6}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Objectif \IeC {\`a} long terme}{6}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}M\IeC {\'e}thodologie}{7}{subsubsection.1.2.3}
\contentsline {subsubsection}{\numberline {1.2.4}Positionnement scientifique}{7}{subsubsection.1.2.4}
\contentsline {section}{\numberline {2}Stage}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}Pr\IeC {\'e}sentation du stage}{8}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Cr\IeC {\'e}ation de la base du mod\IeC {\`e}le}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Premi\IeC {\`e}re experience sur le mod\IeC {\`e}le 2 moteurs pas de capteur}{11}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Seconde exp\IeC {\'e}rience avec 1 capteur et 1 moteur}{12}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5} Troisi\IeC {\`e}me exp\IeC {\'e}rience 2 moteurs et 2 capteurs}{14}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Derni\IeC {\`e}re experience sur l'emergence de comportement}{20}{subsection.2.6}
\contentsline {section}{\numberline {3}Conclusion du stage}{21}{section.3}
